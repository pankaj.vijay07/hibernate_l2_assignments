package trendNxt_HibernateL2.Assignment2b;
/*
 * When a new employee joins the company, the employee record is created in the 
 * employee table.  EmpCode should be auto generated using sequences. 
 
Perform following operations on above table: 
 
b. Enable second level cache(EHCache) with read-write strategy
 to save employee objects data.
 * 
 * 
 */

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class ManageEmployeeData {

	private static SessionFactory factory;

	public static void main(String[] args) {

		factory = new Configuration().configure().buildSessionFactory();

		/* Add employee records in batches */
		new ManageEmployeeData().addEmployeesData();

		Session session1 = factory.openSession();
		Employee emp1 = (Employee) session1.load(Employee.class, 12);
		System.out.println(emp1.getEmpID() + " " + emp1.getEmpName() + " " + emp1.getEmpSalary());
		session1.close();

		Session session2 = factory.openSession();
		Employee emp2 = (Employee) session2.load(Employee.class, 12);
		System.out.println(emp2.getEmpID() + " " + emp2.getEmpName() + " " + emp2.getEmpSalary());
		session2.close();

	}

	/* Method to insert employee data(records) in batches */
	public void addEmployeesData() {
		Session session = factory.openSession();
		Transaction tx = null;

		try {
			tx = session.beginTransaction();
			for (int i = 0; i < 20; i++) {
				String eName = "name " + (i + 1);
				String edesignation = "de" + (i + 1);
				String edob = "198" + i + "-08-13";
				String edoj = "200" + i + "-12-25";
				int eage = i;
				float esalary = (20000 + i) * 1.0f;

				Employee empl = new Employee();
				empl.setEmpName(eName);
				empl.setEmpDesignation(edesignation);
				empl.setEmpDob(edob);
				empl.setEmpDoj(edoj);
				empl.setEmpSalary(esalary);
				empl.setEmpAge(eage);
				session.save(empl);
				if (i % 10 == 0) {
					session.flush();
					session.clear();
				}
			}
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return;
	}
}
