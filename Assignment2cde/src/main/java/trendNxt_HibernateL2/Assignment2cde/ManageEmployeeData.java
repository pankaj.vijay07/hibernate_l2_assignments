package trendNxt_HibernateL2.Assignment2cde;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

public class ManageEmployeeData {

	
	private static SessionFactory factory;

	public static void main(String[] args) {

		factory = new Configuration().configure().buildSessionFactory();

		/* Add employee records in batches */
		new ManageEmployeeData().addEmployeesData();
		
		System.out.println("--------------------x------------x-------------------");
		
		new ManageEmployeeData().getRecordsBasedOnSalaryCriterion();
		
		System.out.println("--------------------x------------x--------------------");
		
		new ManageEmployeeData().getRecordsBasedOnLetterCriterion();
		
		System.out.println("--------------------x------------x---------------------");
		
		new ManageEmployeeData().getRecordsUsingScalarQuery();
		
		System.out.println("--------------------x------------x----------");

		new ManageEmployeeData().getRecordsUsingEntityQuery();;	
	}

	/* Method to insert employee data(records) in batches */
	public void addEmployeesData() {
		Session session = factory.openSession();
		Transaction tx = null;

		try {
			tx = session.beginTransaction();

				Employee emp1=new Employee("Mitali Raj","PM"
						,"1985-02-12", "2007-07-16", 23,22780);
				session.save(emp1);
				Employee emp2=new Employee("Rahul Kumar","SME"
						,"1983-04-12", "2005-07-16", 25,40780);
				session.save(emp2);
				Employee emp3=new Employee("Karthik Sirwani","SEx"
						,"1990-09-12", "2011-07-16", 21,30780);
				session.save(emp3);
				Employee emp4=new Employee("Pankaj Vijay","PEng"
						,"1994-10-14", "2017-03-30", 22,9780);
				session.save(emp4);
				Employee emp5=new Employee("Richa Jain","WMG"
						,"1996-10-12", "2018-09-16", 22,42780);
				session.save(emp5);
				Employee emp6=new Employee("Shubham Kumar","Engg"
						,"1993-01-10", "2017-12-30", 23,9280);
				session.save(emp6);
				tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return;
	}

	
	void getRecordsBasedOnSalaryCriterion(){
		List<Employee> list;
		Session session=factory.openSession();
		Transaction tx=session.beginTransaction();
		Criteria criterion=session.createCriteria(Employee.class);
		criterion.add(Restrictions.between("empSalary", 10000, 40000));
		list=criterion.list();
		for(Employee e:list){
			System.out.println(e);
		}
		tx.commit();
		session.close();
		
	}
	
	void getRecordsBasedOnLetterCriterion(){
		List<Employee> list;
		Session session=factory.openSession();
		Transaction tx=session.beginTransaction();
		Criteria criterion=session.createCriteria(Employee.class);
		criterion.add(Restrictions.ilike("empName", "%r", MatchMode.END));
		list=criterion.list();
		for(Employee e:list){
			System.out.println(e);
		}
		tx.commit();
		session.close();
		
	}
	
	void getRecordsUsingScalarQuery(){
		
		Session session=factory.openSession();
		Transaction tx = session.beginTransaction();
		SQLQuery q=session.createSQLQuery("SELECT empName,empSalary from EMPLOYEE");
		q.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		List list=q.list();
		for(Object obj:list){
			Map row = (Map)obj;
	           System.out.print("Employee Name: " + row.get("empName")); 
	           System.out.println("Salary: " + row.get("empSalary")); 
		}
		
		tx.commit();
		session.close();	
	}
	
	void getRecordsUsingEntityQuery(){
		
		Session session=factory.openSession();
		Transaction tx=session.beginTransaction();
		String sql = "SELECT * FROM EMPLOYEE";
		SQLQuery query = session.createSQLQuery(sql);
		query.addEntity(Employee.class);
		List results = query.list();
		
		for (Iterator iterator = results.iterator(); iterator.hasNext();){
            Employee employee = (Employee) iterator.next(); 
            System.out.print("Employee Name: " + employee.getEmpName()); 
            System.out.print("Empoyee Designation: " + employee.getEmpDesignation()); 
            System.out.println("Employee Salary: " + employee.getEmpSalary()); 
            System.out.println("Employee Age: " + employee.getEmpAge()); 
         }
         tx.commit();
	session.close();	
	}
}
