package trendNxt_HibernateL2.Assignment2cde;

public class Employee {

	private int empID;
	private String empName;
	private String empDesignation;
	private String empDob;
	private String empDoj;
	private int empAge;
	private int empSalary;


	public Employee() {
		super();
	}
	public Employee(String empName, String empDesignation, String empDob, String empDoj, int empAge,
			int empSalary) {
		super();
		this.empName = empName;
		this.empDesignation = empDesignation;
		this.empDob = empDob;
		this.empDoj = empDoj;
		this.empAge = empAge;
		this.empSalary = empSalary;
	}
	public int getEmpID() {
		return empID;
	}
	public void setEmpID(int empID) {
		this.empID = empID;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public String getEmpDesignation() {
		return empDesignation;
	}
	public void setEmpDesignation(String empDesignation) {
		this.empDesignation = empDesignation;
	}
	public String getEmpDob() {
		return empDob;
	}
	public void setEmpDob(String empDob) {
		this.empDob = empDob;
	}
	public String getEmpDoj() {
		return empDoj;
	}
	public void setEmpDoj(String empDoj) {
		this.empDoj = empDoj;
	}
	public int getEmpAge() {
		return empAge;
	}
	public void setEmpAge(int empAge) {
		this.empAge = empAge;
	}
	public int getEmpSalary() {
		return empSalary;
	}
	public void setEmpSalary(int empSalary) {
		this.empSalary = empSalary;
	}
	@Override
	public String toString() {
		return "Employee [empID=" + empID + ", empName=" + empName + ", empDesignation=" + empDesignation + ", empDob="
				+ empDob + ", empDoj=" + empDoj + ", empAge=" + empAge + ", empSalary=" + empSalary + "]";
	}
	
}
