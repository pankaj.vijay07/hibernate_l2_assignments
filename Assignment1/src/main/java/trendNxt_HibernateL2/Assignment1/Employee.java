package trendNxt_HibernateL2.Assignment1;

public class Employee {

	private int employeeID;

	private String employeeName;

	private float employeeSalary;

	private String employeeDOJ;

	public Employee() {
		super();
	}

	public Employee(int employeeID, String employeeName, float employeeSalary, String employeeDOJ) {
		super();
		this.employeeID = employeeID;
		this.employeeName = employeeName;
		this.employeeSalary = employeeSalary;
		this.employeeDOJ = employeeDOJ;
	}

	public int getEmployeeID() {
		return employeeID;
	}

	public void setEmployeeID(int employeeID) {
		this.employeeID = employeeID;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public float getEmployeeSalary() {
		return employeeSalary;
	}

	public void setEmployeeSalary(float employeeSalary) {
		this.employeeSalary = employeeSalary;
	}

	public String getEmployeeDOJ() {
		return employeeDOJ;
	}

	public void setEmployeeDOJ(String employeeDOJ) {
		this.employeeDOJ = employeeDOJ;

	}

}
