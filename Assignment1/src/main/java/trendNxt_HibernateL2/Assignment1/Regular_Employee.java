package trendNxt_HibernateL2.Assignment1;

public class Regular_Employee extends Employee {

	private int qplc;

	public Regular_Employee() {
		super();
	}

	public Regular_Employee(int qplc) {
		super();
		this.qplc = qplc;
	}

	public int getQplc() {
		return qplc;
	}

	public void setQplc(int qplc) {
		this.qplc = qplc;
	}

}
