package trendNxt_HibernateL2.Assignment1;
/*
 * Write an Employee class with the Members employeeID, employeeName, employeeSalary and employeeDOJ. 
	Implement a Two subclass by name RegularEmployee with the member name qplc and ContractEmployee with the member name allowance.
	Write a client class such that, when you save/delete Regular Employee or Contract employee class object, 
	appropriate information should be stored or deleted from the related table using “Table per concrete class” approach.
	*/


import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class ClientClass {

	private static SessionFactory factory;

	public static void main(String[] args) {

		factory = new Configuration().configure().buildSessionFactory();

		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();

		Employee emp = new Employee();
		emp.setEmployeeName("Pawan Singh");
		emp.setEmployeeSalary(32000f);
		emp.setEmployeeDOJ("2016-12-25");
		session.save(emp);

		Regular_Employee rgEmp = new Regular_Employee();
		rgEmp.setEmployeeName("Amit Sharma");
		rgEmp.setEmployeeSalary(35000);
		rgEmp.setEmployeeDOJ("2016-12-25");
		rgEmp.setQplc(9000);
		session.save(rgEmp);

		Contract_Employee cntEmp = new Contract_Employee();
		cntEmp.setEmployeeName("Sakshi Malhotra");
		cntEmp.setEmployeeSalary(21000);
		cntEmp.setEmployeeDOJ("2016-09-15");
		cntEmp.setAllowance(6500);
		session.save(cntEmp);

		Contract_Employee cntEmp1 = new Contract_Employee();
		cntEmp1.setEmployeeName("Nehal Pareek");
		cntEmp1.setEmployeeSalary(43000);
		cntEmp1.setEmployeeDOJ("2015-10-15");
		cntEmp1.setAllowance(8500);
		session.save(cntEmp1);

		tx.commit();
		session.close();

		new ClientClass().listContractEmployees();

		// new ClientClass().deleteContractEmployee(3);

		// new ClientClass().listContractEmployees();

		System.out.println("Success");

	}

	/* Method to DELETE Contract employee from the records */
	public void deleteContractEmployee(Integer empID) {

		factory = new Configuration().configure().buildSessionFactory();
		Session session = factory.openSession();
		Transaction tx = null;

		try {
			tx = session.beginTransaction();
			Contract_Employee contract_employee = (Contract_Employee) session.get(Contract_Employee.class, empID);
			session.delete(contract_employee);
			tx.commit();
		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {
			session.close();
		}

		System.out.println("Record deleted successfully");
	}

	// delete Regular employee object
	public void deleteRegularEmployee(Integer empID) {

		factory = new Configuration().configure().buildSessionFactory();
		Session session = factory.openSession();
		Transaction tx = null;

		try {
			tx = session.beginTransaction();
			Regular_Employee reg_employee = (Regular_Employee) session.get(Regular_Employee.class, empID);
			session.delete(reg_employee);
			tx.commit();
		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
		System.out.println("Record deleted successfully");
	}

	/* Method to list all the employees detail */
	public void listContractEmployees() {

		factory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();

		Session session = factory.openSession();
		Transaction tx = null;

		try {
			tx = session.beginTransaction();
			List employees = session.createQuery("FROM Contract_Employee").list();
			for (Iterator iterator = employees.iterator(); iterator.hasNext();) {
				Contract_Employee contrt_employee = (Contract_Employee) iterator.next();
				System.out.print("Employee Name: " + contrt_employee.getEmployeeName());
				System.out.println("  Salary: " + contrt_employee.getEmployeeSalary());
				System.out.println(" Date of Joining: " + contrt_employee.getEmployeeDOJ());
			}
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	public void listRegularEmployees() {

		factory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();

		Session session = factory.openSession();
		Transaction tx = null;

		try {
			tx = session.beginTransaction();
			List employees = session.createQuery("FROM Regular_Employee").list();
			for (Iterator iterator = employees.iterator(); iterator.hasNext();) {
				Regular_Employee reg_employee = (Regular_Employee) iterator.next();
				System.out.print("Employee Name: " + reg_employee.getEmployeeName());
				System.out.println("  Salary: " + reg_employee.getEmployeeSalary());
				System.out.println("  Salary: " + reg_employee.getEmployeeDOJ());
			}
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

}
