package trendNxt_HibernateL2.Assignment1;

public class Contract_Employee extends Employee {

	private int allowance;

	public Contract_Employee(int allowance) {
		super();
		this.allowance = allowance;
	}

	public Contract_Employee() {
		super();
	}

	public int getAllowance() {
		return allowance;
	}

	public void setAllowance(int allowance) {
		this.allowance = allowance;
	}

}
