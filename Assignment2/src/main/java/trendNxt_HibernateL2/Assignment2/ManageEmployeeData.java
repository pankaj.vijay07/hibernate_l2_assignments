package trendNxt_HibernateL2.Assignment2;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class ManageEmployeeData {

		 private static SessionFactory factory; 
		   public static void main(String[] args) {
		      
		     
		         factory = new Configuration().configure().buildSessionFactory();
		      
		      

		      /* Add employee records in batches */
		      new ManageEmployeeData().addEmployeesData( );
		   }
		   
		   /* Method to insert employee data(records) in batches */
		   public void addEmployeesData( ){
		      Session session = factory.openSession();
		      Transaction tx = null;
		      
		      try {
		         tx = session.beginTransaction();
		         for (int i=0; i<50; i++ ) {
		            String eName = "name " + (i+1);
		            String edesignation = "de" + (i+1);
		            String edob = "198"+i+"-08-13";
		            String edoj = "200"+i+"-12-25";
		            int eage = i;
		            float esalary = (20000+i)*1.0f;
		       
		            Employee empl =new Employee();
		            empl.setEmpName(eName);
		            empl.setEmpDesignation(edesignation);
		            empl.setEmpDob(edob);
		            empl.setEmpDoj(edoj);
		            empl.setEmpSalary(esalary);
		            empl.setEmpAge(eage);
		            session.save(empl);
		         	if( i % 10 == 0 ) {
		               session.flush();
		               session.clear();
		            }
		         }
		         tx.commit();
		      } catch (HibernateException e) {
		         if (tx!=null) tx.rollback();
		         e.printStackTrace(); 
		      } finally {
		         session.close(); 
		      }
		      return ;
		   }
}
