package trendNxt_HibernateL2.Assignment2;

public class Employee {

private int empID;
private String empName;
private String empDesignation;
private String empDob;
private String empDoj;
private int empAge;
private float empSalary;


public Employee() {
	super();
}
public Employee(int empID, String empName, String empDesignation, String empDob, String empDoj, int empAge,
		float empSalary) {
	super();
	this.empID = empID;
	this.empName = empName;
	this.empDesignation = empDesignation;
	this.empDob = empDob;
	this.empDoj = empDoj;
	this.empAge = empAge;
	this.empSalary = empSalary;
}
public int getEmpID() {
	return empID;
}
public void setEmpID(int empID) {
	this.empID = empID;
}
public String getEmpName() {
	return empName;
}
public void setEmpName(String empName) {
	this.empName = empName;
}
public String getEmpDesignation() {
	return empDesignation;
}
public void setEmpDesignation(String empDesignation) {
	this.empDesignation = empDesignation;
}
public String getEmpDob() {
	return empDob;
}
public void setEmpDob(String empDob) {
	this.empDob = empDob;
}
public String getEmpDoj() {
	return empDoj;
}
public void setEmpDoj(String empDoj) {
	this.empDoj = empDoj;
}
public int getEmpAge() {
	return empAge;
}
public void setEmpAge(int empAge) {
	this.empAge = empAge;
}
public float getEmpSalary() {
	return empSalary;
}
public void setEmpSalary(float empSalary) {
	this.empSalary = empSalary;
}


}
